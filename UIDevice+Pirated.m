//
//  UIDevice+Pirated.m
//  food
//
//  Created by Whirlwind James on 12-2-18.
//  Copyright (c) 2012年 BOOHEE. All rights reserved.
//
//  Version: 0.1 Update: 2012/4/24

#import "UIDevice+Pirated.h"

@implementation UIDevice (Pirated)

- (BOOL)isPirated{
    BOOL s = NO;
    FILE *pipe = popen([@"ls" cStringUsingEncoding: NSASCIIStringEncoding], "r");
    if (pipe)
    {
        s = YES;
    }
    pclose(pipe);
#ifndef __OPTIMIZE__
    if (s) {
        NSLog(@"my iOS isPirated!");
    }else {
        NSLog(@"my iOS is NOT pirated!");
    }
#endif
	return s;
}
@end
