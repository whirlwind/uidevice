//
//  UIDevice+Pirated.h
//  food
//
//  Created by Whirlwind James on 12-2-18.
//  Copyright (c) 2012年 BOOHEE. All rights reserved.
//
//  Version: 0.1 Update: 2012/4/24



@interface UIDevice (Pirated)
- (BOOL)isPirated;
@end
