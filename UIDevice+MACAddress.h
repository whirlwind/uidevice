//
//  UIDevice+MACAddress.h
//  food
//
//  Created by Whirlwind James on 11-10-19.
//  Copyright (c) 2011年 BOOHEE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (MACAddress)
- (NSString *) macAddress;
/*
 * @method uniqueDeviceIdentifier
 * @description use this method when you need a unique identifier in one app.
 * It generates a hash from the MAC-address in combination with the bundle identifier
 * of your app.
 */

- (NSString *) uniqueDeviceIdentifier;

/*
 * @method uniqueGlobalDeviceIdentifier
 * @description use this method when you need a unique global identifier to track a device
 * with multiple apps. as example a advertising network will use this method to track the device
 * from different apps.
 * It generates a hash from the MAC-address only.
 */

- (NSString *) uniqueGlobalDeviceIdentifier;
@end
